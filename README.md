Dialogflow:

* Analytics
* Connect with most popular platforms
* Export/Import behaviors
* Training in real-time
* Test in real-time
* Can be used with text or voice

Some diagrams:

![My Boiler](https://i.imgur.com/nOGwb1J.png)

![Marie Digital Assistant](https://i.imgur.com/GXEJujq.png)

Goals:

* 500 orders at the end of 2017 4Q

Proposed objectives:

* Use Facebook platform
* Only home payment
* Provide this service to one restaurant only, since menu can be very different

Future:

* Sentiment analysis for real agent to take over in case user is not happy with the service.

Links:

* https://dialogflow.com
* https://developers.google.com/actions/dialogflow/first-app
* https://www.youtube.com/watch?v=K4v_QnngRdg
* https://www.youtube.com/watch?v=LHX1GqZuaII
* https://www.youtube.com/watch?v=5iKdfPjEOJk